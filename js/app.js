function clearResults() {
    let div = document.getElementById('results');
    div.innerHTML = '';
}

function showResults(results) {
    let div = document.getElementById('results');

    clearResults();

    for (let i = 0; i < results.length; i++) {
        let p = document.createElement('p');
        p.innerHTML = results[i].first_name + ' ' + results[i].last_name;

        div.appendChild(p);
    }
}

function updateResults(terms, dupes) {
    const url = API_URL + '/find_users?terms=' + terms + '&dupes=' + dupes;

    fetch(url)
        .then(function (result) {
            return result.json();
        })
        .then(function (data) {
            showResults(data);
        });
}

function findUsers(e) {
    let terms = document.getElementById('terms').value;
    let dupes = document.getElementById('dupes').checked ? 1 : 0;

    updateResults(terms, dupes);

    e.preventDefault();
}

form = document.getElementById('terms_form');
form.addEventListener('submit', findUsers);
